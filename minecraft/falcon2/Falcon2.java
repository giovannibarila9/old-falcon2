package falcon2;

import falcon2.file.FileManager;
import falcon2.gui.*;
import falcon2.modules.ModuleManager;
import falcon2.modules.commands.CommandManager;
import falcon2.settings.SettingsManager;

public class Falcon2 {
	private static CommandManager commandManager = new CommandManager();
	private static FileManager fileManager;
	private static Fonts fonts = new Fonts();
	private static GuiInGame guiInGame = new GuiInGame();
	private static KeybindManager keybindManager = new KeybindManager();
	private static ModuleManager moduleManager = new ModuleManager();
	private static SettingsManager settingsManager;
	
	public static CommandManager getCommandManager() {
		return commandManager;
	}
	
	public static FileManager getFileManager() {
		return fileManager;
	}
	
	public static Fonts getFonts() {
		return fonts;
	}
	
	public static GuiInGame getGuiInGame() {
		return guiInGame;
	}
	
	public static KeybindManager getKeybindManager() {
		return keybindManager;
	}
	
	public static ModuleManager getModuleManager() {
		return moduleManager;
	}
	
	public static SettingsManager getSettingsManager() {
		return settingsManager;
	}
	
	public static void setFileManager() {
		if (fileManager != null)
			return;
		fileManager = new FileManager();
	}
	
	public static void setSettingsManager() {
		if (settingsManager != null)
			return;
		settingsManager = new SettingsManager();
	}
}