package falcon2;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.multiplayer.PlayerControllerMP;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.network.play.client.C01PacketChatMessage;
import net.minecraft.util.ChatComponentText;

public class Wrapper {	
	public static Minecraft getMinecraft() {
		return Minecraft.getMinecraft();
	}
	
	public static EntityClientPlayerMP getPlayer() {
		return getMinecraft().thePlayer;
	}
	
	public static WorldClient getWorld() {
		return getMinecraft().theWorld;
	}
	
	public static NetHandlerPlayClient getSendQueue() {
		return getPlayer().sendQueue;
	}
	
	public static void sendChatMessage(String s) {
		getSendQueue().addToSendQueue(new C01PacketChatMessage(s));
	}
	
	public static void addChatMessage(String s) {
		getPlayer().addChatMessage(new ChatComponentText("[F\247c2\247f]: " + s));
	}
	
	public static void addChatMessageError(String s) {
		getPlayer().addChatMessage(new ChatComponentText("[F\247c2\247f] [\247cError\247f]: " + s));
	}
	
	public static FontRenderer getFontRenderer() {
		return getMinecraft().fontRenderer;
	}
	
	public static Long getSysTime() {
		return System.nanoTime() / 1000000;
	}
	
	public static PlayerControllerMP getController() {
		return getMinecraft().playerController;
	}
	
	public static GameSettings getGameSettings() {
		return getMinecraft().gameSettings;
	}
}