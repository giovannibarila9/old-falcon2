package falcon2.gui.dragui;

import static falcon2.Fonts.font1;
import static falcon2.Fonts.font2;

import java.util.ArrayList;

import falcon2.Falcon2;
import falcon2.Wrapper;
import falcon2.gui.CustomFont;
import falcon2.gui.dragui.item.Item;
import static falcon2.gui.Gui.*;
import falcon2.modules.Category;
import falcon2.modules.Module;

public class Panel {
	private String title;
	private Category category;
	private int x;
	private int y;
	private int x2;
	private int y2;
	private int width;
	private int height;
	private boolean open;
	private boolean drag;
	private ArrayList<Item> items = new ArrayList<Item>();
	private CustomFont font;
	
	public Panel(String title, Category category, int x, int y, int width, int height) {
		this.title = title;
		this.category = category;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		font = Falcon2.getFonts().font1;
		setupItems();
	}
	
	public void setupItems() {
		
	}
	
	public void drawScreen(int i, int j, float k) {
		drag(i, j);
		int totalItemHeight = open ? (getTotalItemHeight() - 1) : 0;
		drawRectWithBorder(getX(), getY(), getX() + getWidth(), getY() + getHeight() + totalItemHeight, 0x60000000, 0xaf000000, 0.5F);
		drawRectWithBorder(getX() + 0.5F, getY() + 0.5F, (getX() + getWidth()) - 0.5F, (getY() + getHeight() + totalItemHeight) - 0.5F, 0x00, 0x50555555, 0.5F);
		font.drawStringWithShadow(category.name(), getX() + 4, getY() + 3, 0xffffffff);
		if (open) {
			int y = getY() + getHeight() - 2;
			for (Item item : getItems()) {
				item.setLocation(x + 2, y);
				item.setWidth(getWidth() - 4);
				item.drawScreen(i, j, k);
				y += item.getHeight() + 1;
			}
		}
	}
	
	private void drag(int i, int j) {
		if (!drag)
			return;
		x = x2 + i;
		y = y2 + j;
	}
	
	public void mouseClicked(int i, int j, int k) {
		if (k == 0 && isHovering(i, j)) {
			x2 = x - i;
			y2 = y - j;
			drag = true;
			return;
		}
		if (k == 1 && isHovering(i, j)) {
			open = !open;
			return;
		}
		if (!open)
			return;
		for (Item item : getItems())
			item.mouseClicked(i, j, k);
	}
	
	public void mouseMovedOrUp(int i, int j, int k) {
		if (k == 0) {
			drag = false;
		}
		if (!open)
			return;
		for (Item item : getItems())
			item.mouseMovedOrUp(i, j, k);
	}

	public Category getCategory() {
		return category;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
	
	public boolean getOpen() {
		return open;
	}
	
	public ArrayList<Item> getItems() {
		return items;
	}
	
	public boolean isHovering(int i, int j) {
		return i >= getX() && i <= getX() + getWidth() && j >= getY() && j <= getY() + getHeight() - (open ? 2 : 0);
	}
	
	public int getTotalItemHeight() {
		int height = 0;
		for (Item item : getItems())
			height += item.getHeight() + 1;
		return height;
	}
}
