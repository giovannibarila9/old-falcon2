package falcon2.gui.dragui.item;

import static falcon2.gui.Gui.*;
import falcon2.Falcon2;

public class ItemSlider extends Item {
	private String title;
	private int x2;
	private int y2;
	private float minX;
	private float maxX;
	private float value;
	private boolean drag;
	
	public ItemSlider(String title, float minX, float maxX, float value) {
		this.title = title;
		this.minX = minX;
		this.maxX = maxX;
		this.value = value - minX;
	}
	
	@Override
	public void drawScreen(int i, int j, float k) {
		drag(i, j);
		drawRectWithBorder(getX(), getY(), getX() + getWidth(), getY() + getHeight(), 0x30333333, 0xaf000000, 0.5F);
		drawGradientRectWithBorder(getX(), getY(), getX() + getWidth(), getY() + getHeight(), 0x40000000, 0x00, 0x00, 0.5F);
		float val = (value / (maxX - minX)) * getWidth();
		if (val > 0.0F) {
			drawGradientRectWithBorder(getX(), getY(), getX() + val, getY() + getHeight(), 0x30555555, 0x30222222, 0x00, 0.5F);
			drawRectWithBorder(getX() + 0.5F, getY() + 0.5F, (getX() + val) - 0.5F, (getY() + getHeight()) - 0.5F, 0x00, 0x30555555, 0.5F);
		}
		String str = title + ": \247c" + (value + minX);
		int strWidth = Falcon2.getFonts().font1b.getStringWidth(str);
		Falcon2.getFonts().font1b.drawStringWithShadow(str, getX() + (getWidth() / 2) - (strWidth / 2), getY() + 5, 0xff999999);
	}
	
	private void drag(int i, int j) {
		if (drag) {
			float val = ((float) (i - getX()) / getWidth()) * (maxX - minX);
			try {
				System.out.println(val + "");
				String val2[] = (val + "").replace(".", "/").split("/");
				val = Float.parseFloat(val2[0] + "." + val2[1].substring(0, 1));
			} catch (Exception e) {
				e.printStackTrace();
			}
			value = val;
		}
		if (value < 0.0F)
			value = 0.0F;
		if (value > (maxX - minX))
			value = (maxX - minX);
	}
	
	@Override
	public void mouseClicked(int i, int j, int k) {
		if (k == 0 && isHovering(i, j)) {
			x2 = x - i;
			drag = true;
		}
	}
	
	@Override
	public int getHeight() {
		return 14;
	}
	
	@Override
	public void mouseMovedOrUp(int i, int j, int k) {
		if (k == 0)
			drag = false;
	}
	
	public boolean isHovering(int i, int j) {
		return i >= getX() && i <= getX() + getWidth() && j >= getY() && j <= getY() + getHeight();
	}
	
	public float getValueFloat() {
		return (value + minX);
	}
	
	public int getValueInt() {
		return (int) getValueFloat();
	}
}