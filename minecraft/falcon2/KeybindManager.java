package falcon2;

import org.lwjgl.input.Keyboard;
import falcon2.gui.GuiDragUI;
import falcon2.modules.Module;
import static falcon2.Wrapper.*;

public class KeybindManager {
	public void runKey(Integer key) {
		if (key == Keyboard.KEY_RSHIFT) {
			getMinecraft().displayGuiScreen(new GuiDragUI());
			return;
		}
		for (Module module : Falcon2.getModuleManager().getModules()) {
			if (module.getKeybind() == key && !module.getHidden())
				module.toggle();
		}
	}
}
