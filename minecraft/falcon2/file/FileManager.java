package falcon2.file;

import java.io.File;
import java.util.ArrayList;
import falcon2.file.files.*;
import falcon2.modules.*;

public class FileManager {
	private static FileInstances fileInstances;
	
	public FileManager() {
		fileInstances = new FileInstances();
		loadFiles();
	}
	
	public void saveFiles() {
		for (FileHandler fileHandler : fileInstances.getFileHandlers())
			fileHandler.saveFile();
	}
	
	public void saveFile(FileHandler fileHandler) {
		fileHandler.saveFile();
	}
	
	public void loadFiles() {
		for (FileHandler fileHandler : fileInstances.getFileHandlers())
			fileHandler.loadFile();
	}
	
	public void loadFile(FileHandler fileHandler) {
		fileHandler.loadFile();
	}
	
	public ArrayList<FileHandler> getFileHandlers() {
		return fileInstances.getFileHandlers();
	}
	
	public FileInstances getFileInstances() {
		return fileInstances;
	}
}