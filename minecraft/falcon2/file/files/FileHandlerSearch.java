package falcon2.file.files;

import java.awt.Color;
import java.io.File;
import java.util.ArrayList;
import falcon2.file.FileHandler;
import falcon2.file.FileUtils;
import falcon2.modules.Search;

public class FileHandlerSearch extends FileHandler {
	public FileHandlerSearch() {
		super(new File("search.txt"));
	}
	
	@Override
	public void saveFile() {
		ArrayList<String> contents = new ArrayList<String>();
		for (Search.SearchBlock searchBlock : Search.blocks) {
			int red = searchBlock.getColor().getRed();
			int green = searchBlock.getColor().getGreen();
			int blue = searchBlock.getColor().getBlue();
			contents.add(searchBlock.getID() + ":" + (red + "," + green + "," + blue) + "\r\n");
		}
		FileUtils.writeFile(getFile(), contents);
	}
	
	@Override
	public void loadFile() {
		if (!doesFileExist())
			return;
		ArrayList<String> contents = FileUtils.readFile(getFile());
		for (String line : contents) {
			line = line.trim();
			if (line.length() > 0) {
				String split[] = line.split(":");
				String split2[] = split[1].split(",");
				float red = Float.parseFloat(split2[0]) / 255F;
				float green = Float.parseFloat(split2[1]) / 255F;
				float blue = Float.parseFloat(split2[1]) / 255F;
				Search.blocks.add(new Search.SearchBlock(Integer.parseInt(split[0]), new Color(red, green, blue)));
			}
		}
	}
}
