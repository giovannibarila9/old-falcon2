package falcon2.settings;

public class Setting {
	private String settingName;
	private SettingType settingType;
	
	public Setting(String settingName, SettingType settingType) {
		this.settingName = settingName;
		this.settingType = settingType;
	}
	
	public String getSettingName() {
		return settingName;
	}
	
	public SettingType getSettingType() {
		return settingType;
	}
	
	public Object getSetting() {
		return null;
	}
	
	public void setSetting(Object setting) {
		
	}
	
	public static enum SettingType {
		BOOLEAN, DOUBLE, FLOAT, INTEGER
	}
}
