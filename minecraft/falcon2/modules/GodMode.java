package falcon2.modules;

import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.network.play.client.C0BPacketEntityAction;
import org.lwjgl.input.Keyboard;
import falcon2.modules.events.*;

public class GodMode extends Module {
	private static int delay = 0;
	private static boolean set = false;
	
	public GodMode() {
		super("GodMode", Keyboard.KEY_G, Category.COMBAT, false);
	}
	
	@Override
	public void onEvent(Event event) {
		if (!getEnabled())
			return;
		if (event instanceof EventOnUpdate) {
			EventOnUpdate event2 = (EventOnUpdate) event;
			if (event2.getState() == State.POST) {
				delay++;
				boolean still = getPlayer().movementInput.moveForward == 0.0F;
				if (delay >= 61) {
					if (delay == 61)
						getSendQueue().addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(getPlayer().posX, getPlayer().boundingBox.minY + 0.125D, getPlayer().posY + 0.125D, getPlayer().posZ, getPlayer().onGround));
					delay = 0;
				} else if (delay >= 60) {
					getSendQueue().addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(getPlayer().posX, getPlayer().boundingBox.minY - 0.125D, getPlayer().posY - 0.125D, getPlayer().posZ, getPlayer().onGround));
				}
			}
		}
	}
	
	@Override
	public void onEnabled() {
		delay = 0;
		set = false;
		super.onEnabled();
	}
	
	@Override
	public void onDisabled() {
		super.onDisabled();
		if (set) {
			getPlayer().setSprinting(false);
			getGameSettings().keyBindForward.pressed = false;
			set = false;
		}
		getSendQueue().addToSendQueue(new C0BPacketEntityAction(getPlayer(), 2));
	}
}
