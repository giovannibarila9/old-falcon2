package falcon2.modules;

import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.network.play.client.C0APacketAnimation;
import net.minecraft.network.play.client.C0BPacketEntityAction;
import org.lwjgl.input.Keyboard;
import falcon2.modules.events.*;

public class Retard extends Module {
	private float aimYaw, aimPitch;
	
	public Retard() {
		super("Retard", Keyboard.KEY_H, Category.MOVEMENT, false);
	}
	
	@Override
	public void onEvent(Event event) {
		if (getModuleByName("Aura").getEnabled() && getModuleByName("AimBot").getEnabled() && AimBot.aimYaw != null && AimBot.aimPitch != null && Aura.target != null)
			return;
		if (!getEnabled())
			return;
		if (event instanceof EventRotation) {
			boolean moving = getPlayer().movementInput.moveForward != 0.0F;
			EventRotation event2 = (EventRotation) event;
			aimYaw += 300;
			aimPitch = getPlayer().rotationPitch;
			event2.setRotation(aimYaw, aimPitch);
			getSendQueue().addToSendQueue(new C0APacketAnimation(getPlayer(), 1));
			getSendQueue().addToSendQueue(new C0BPacketEntityAction(getPlayer(), moving ? 2 : 1));
		}
	}
	
	@Override
	public void onDisabled() {
		super.onDisabled();
		getSendQueue().addToSendQueue(new C0BPacketEntityAction(getPlayer(), 2));
	}
}
