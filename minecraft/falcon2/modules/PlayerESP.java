package falcon2.modules;

import org.lwjgl.opengl.GL11;
import falcon2.Falcon2;
import falcon2.modules.events.*;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.*;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.StringUtils;

public class PlayerESP extends Module {
	public PlayerESP() {
		super("PlayerESP", null, Category.RENDER, false);
	}

	@Override
	public void onEvent(Event event) {
		if (!getEnabled())
			return;
		if (event instanceof EventRender) {
			for (Entity entity : getWorld().loadedEntityList) {
				if (entity != getPlayer() && entity instanceof EntityLivingBase)
					drawBox(entity, Aura.target);
			}
		}
	}
	
	private void drawBox(Entity entity, Entity target) {
		if (!(entity instanceof EntityPlayer))
			return;
		boolean isAuraTarget = getModuleByName("Aura").getEnabled() && entity == target;
		float xDiff = (float) (entity.posX - RenderManager.renderPosX);
		float yDiff = (float) (entity.boundingBox.minY - RenderManager.renderPosY);
		float zDiff = (float) (entity.posZ - RenderManager.renderPosZ);
		float distance = this.getPlayer().getDistanceToEntity(entity);
		boolean friend = false;
		EntityPlayer entityPlayer = (EntityPlayer) entity;
		String username = StringUtils.stripControlCodes(entityPlayer.getCommandSenderName());
		friend = Friends.checkFriend(username);
		renderUtils.start3DGLConstants();
		GL11.glEnable(GL11.GL_LINE_SMOOTH);
		GL11.glHint(GL11.GL_LINE_SMOOTH_HINT, GL11.GL_FASTEST);
		float red = 0F;
		float green = 0F;
		float blue = 0F;
		float alpha = 0.0F;
		if (isAuraTarget) {
			green = 1F;
			alpha = 0.8F;
			GL11.glColor4f(red, green, blue, alpha);
		} else if (friend) {
			red = 92F / 255F;
			green = 191F / 255F;
			blue = 1F;
			alpha = 0.6F;
			GL11.glColor4f(red, green, blue, alpha);
		} else if (distance >= 64f) {
			green = 1F;
			alpha = 0.4F;
			GL11.glColor4f(red, green, blue, alpha);
		} else {
			red = 1F;
			green = (distance / 64F);
			alpha = 0.4F;
			GL11.glColor4f(red, green, blue, alpha);
		}
		GL11.glLineWidth(isAuraTarget ? 2.5F : 1.5F);
		float width = entity.width / (4F / 3F);
		AxisAlignedBB axisAlignedBB = AxisAlignedBB.getBoundingBox(xDiff - width, yDiff - 0.1F, zDiff - width, xDiff + width, yDiff + entity.height + 0.2F, zDiff + width);
		GL11.glPushMatrix();
		GL11.glTranslatef(xDiff, yDiff, zDiff);
		GL11.glRotatef(entity.rotationYaw, 0F, yDiff, 0F);
		GL11.glTranslatef(-xDiff, -yDiff, -zDiff);
		renderUtils.drawSupportBeams(axisAlignedBB);
		renderUtils.drawOutlinedBoundingBox(axisAlignedBB);
		alpha -= 0.2F;
		GL11.glColor4f(red, green, blue, alpha);
		renderUtils.drawBoundingBox(axisAlignedBB);
		GL11.glPopMatrix();
		GL11.glDisable(GL11.GL_LINE_SMOOTH);
		renderUtils.finish3DGLConstants();
	}
}
