package falcon2.modules;

import java.util.List;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ChatLine;
import net.minecraft.client.gui.Gui;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.MathHelper;
import net.minecraft.util.StringUtils;
import org.lwjgl.opengl.GL11;
import falcon2.Falcon2;
import falcon2.gui.CustomFont;
import falcon2.modules.events.*;
import static falcon2.Fonts.*;

public class TTF extends Module {
	public TTF() {
		super("TTF", null, Category.RENDER, false);
	}

	@Override
	public void onEvent(Event event) {
		if (!getEnabled())
			return;
		if (event instanceof EventChatRender) {
			EventChatRender event2 = (EventChatRender) event;
			event2.setCanceled(true);
			Object[] objects = event2.getObjects();
			drawTTF(
				(Integer) objects[0],
				(Minecraft) objects[1],
				(Integer) objects[2],
				(List) objects[3],
				(Boolean) objects[4],
				(Float) objects[5],
				(Integer) objects[6],
				(Integer) objects[7],
				(Boolean) objects[8]
			);
		}
	}

	public void drawTTF(Integer p_146230_1_, Minecraft field_146247_f, Integer func_146232_i, List field_146253_i, Boolean func_146241_e, Float func_146244_h, Integer func_146228_f, Integer field_146250_j, Boolean field_146251_k) {
		if (field_146247_f.gameSettings.chatVisibility != EntityPlayer.EnumChatVisibility.HIDDEN) {
			int var2 = func_146232_i;
			boolean var3 = false;
			int var4 = 0;
			int var5 = field_146253_i.size();
			float var6 = field_146247_f.gameSettings.chatOpacity * 0.9F + 0.1F;
			if (var5 > 0) {
				if (func_146241_e)
					var3 = true;
				float var7 = func_146244_h;
				int var8 = MathHelper.ceiling_float_int((float)func_146228_f / var7);
				GL11.glPushMatrix();
				GL11.glTranslatef(2.0F, 20.0F, 0.0F);
				GL11.glScalef(var7, var7, 1.0F);
				int var9;
				int var11;
				int var14;
				int longestString = 0;
				int count = 0;
				for (var9 = 0; var9 + field_146250_j < field_146253_i.size() && var9 < var2; ++var9) {
					ChatLine var10 = (ChatLine)field_146253_i.get(var9 + field_146250_j);
					if (var10 != null) {
						var11 = p_146230_1_ - var10.getUpdatedCounter();
						if (var11 < 200 || var3) {
							double var12 = (double)var11 / 200.0D;
							var12 = 1.0D - var12;
							var12 *= 10.0D;
							if (var12 < 0.0D)
								var12 = 0.0D;
							if (var12 > 1.0D)
								var12 = 1.0D;
							var12 *= var12;
							var14 = (int)(255.0D * var12);
							if (var3)
								var14 = 255;
							var14 = (int)((float)var14 * var6);
							++var4;
							if (var14 > 3) {
								String var17 = var10.func_151461_a().getFormattedText();
								var17 = StringUtils.stripControlCodes(var17);
								int curString = font2.getStringWidth(var17);
								if (longestString < curString)
									longestString = curString;
								count++;
							}
						}
					}
				}
				int h = 9;
				if (count > 0) {
					Gui.drawRect(0, -(count * h) - 7, longestString + 9, 0, 0x80000000);
				}
				for (var9 = 0; var9 + field_146250_j < field_146253_i.size() && var9 < var2; ++var9) {
					ChatLine var10 = (ChatLine)field_146253_i.get(var9 + field_146250_j);
					if (var10 != null) {
						var11 = p_146230_1_ - var10.getUpdatedCounter();
						if (var11 < 200 || var3) {
							double var12 = (double)var11 / 200.0D;
							var12 = 1.0D - var12;
							var12 *= 10.0D;
							if (var12 < 0.0D)
								var12 = 0.0D;
							if (var12 > 1.0D)
								var12 = 1.0D;
							var12 *= var12;
							var14 = (int)(255.0D * var12);
							if (var3)
								var14 = 255;
							var14 = (int)((float)var14 * var6);
							++var4;
							if (var14 > 3) {
								byte var15 = 0;
								int var16 = -var9 * h;
								String var17 = var10.func_151461_a().getFormattedText();
								font2.drawStringWithShadow(var17, var15 + 4, var16 - 11, 0xffffffff);
								GL11.glDisable(GL11.GL_ALPHA_TEST);
							}
						}
					}
				}
				if (var3) {
					var9 = field_146247_f.fontRenderer.FONT_HEIGHT;
					GL11.glTranslatef(-3.0F, 0.0F, 0.0F);
					int var18 = var5 * var9 + var5;
					var11 = var4 * var9 + var4;
					int var20 = field_146250_j * var11 / var5;
					int var13 = var11 * var11 / var18;
					if (var18 != var11) {
						var14 = var20 > 0 ? 170 : 96;
						int var19 = field_146251_k ? 13382451 : 3355562;
						Gui.drawRect(0, -var20, 2, -var20 - var13, var19 + (var14 << 24));
						Gui.drawRect(2, -var20, 1, -var20 - var13, 13421772 + (var14 << 24));
					}
				}
				GL11.glPopMatrix();
			}
		}
	}
}
