package falcon2.modules.commands;

public class Command {
	private String command;
	
	public Command(String command) {
		this.command = command;
	}
	
	public String getCommand() {
		return command;
	}
	
	public boolean runCommand(String arguments) {
		return false;
	}
}
