package falcon2.modules;

import net.minecraft.client.Minecraft;
import net.minecraft.inventory.Slot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import falcon2.modules.commands.*;
import falcon2.modules.events.*;

public class AutoSoup extends Module {
	private int BOWL_SOUP = 282;
	private int BOWL_EMPTY = 281;
	private static Long soupLast = null;
	public static long soupDelay = 50L;
	private int threshold = 7;
	private Long setLast = null, setDelay = 400L;
	
	public AutoSoup() {
		super("AutoSoup", null, Category.COMBAT, false);
	}
	
	@Override
	public void onEvent(Event event) {
		if(!getEnabled())
			return;
		setDelay = 125L;
		if(event instanceof EventOnUpdate) {
			if(((EventOnUpdate) event).getState() == State.POST) {
				if(setLast == null || getSysTime() >= setDelay + setLast && BOWL_SOUP == 282)
					handleBowlSetting();
			}
		} else if(event instanceof EventMotionUpdate) {
			if(soupLast == null || getSysTime() >= soupDelay + soupLast) {
				if(((EventMotionUpdate) event).getState() == State.PRE)
					handleSouping();
			}
		}
	}
	
	@Override
	public void loadCommands(CommandManager commandManager) {
		commandManager.getCommands().add(new Command("as") {
			@Override
			public boolean runCommand(String arguments) {
				if (arguments.contains(" ")) {
					String args[] = arguments.split(" ");
					if (args[0].equalsIgnoreCase("delay")) {
						soupDelay = Long.parseLong(args[1]);
						addChatMessage("AutoSoup eat delay set to '\247e" + soupDelay + "\247f'.");
						return true;
					} else if (args[0].equalsIgnoreCase("id")) {
						BOWL_SOUP = Integer.parseInt(args[1]);
						addChatMessage("AutoSoup item set to '\247e" + BOWL_SOUP + "\247f'.");
						return true;
					} else if (args[0].equalsIgnoreCase("threshold")) {
						threshold = Integer.parseInt(args[1]);
						addChatMessage("AutoSoup threshold set to '\247e" + threshold + "\247f'.");
						return true;
					}
				}
				return false;
			}
		});
	}
	
	private void handleSouping() {
		Minecraft mc = Minecraft.getMinecraft();
		if(mc.thePlayer.getHealth() > threshold)
			return;
		Integer firstSoup = getFirstSoup();
		if(firstSoup != null) {
			mc.thePlayer.inventory.currentItem = firstSoup - 36;
			mc.playerController.updateController();
			ItemStack stack = mc.thePlayer.inventoryContainer.getSlot(firstSoup).getStack();
			mc.thePlayer.sendQueue.addToSendQueue(new C08PacketPlayerBlockPlacement(0, 0, 0, -1, stack, -1, -1, -1));
			soupLast = getSysTime();
		}
	}
	
	private Integer getFirstSoup() {
		Minecraft mc = Minecraft.getMinecraft();
		for(int slot = 37; slot <= 44; slot++) {
			Slot itemSlot = mc.thePlayer.inventoryContainer.getSlot(slot);
			if(itemSlot != null) {
				ItemStack stack = itemSlot.getStack();
				if(stack != null && stack.getItem() == Item.getItemById(BOWL_SOUP))
					return slot;
			}
		}
		return null;
	}
	
	private void handleBowlSetting() {
		Minecraft mc = Minecraft.getMinecraft();
		Integer firstBowl = getFirstBowl();
		ItemStack slotForBowls = mc.thePlayer.inventoryContainer.getSlot(37).getStack();
		if(firstBowl != null && (slotForBowls == null || slotForBowls.getItem() == Item.getItemById(BOWL_EMPTY))) {
			mc.playerController.windowClick(0, firstBowl, 0, 0, mc.thePlayer);
			mc.playerController.windowClick(0, 37, 0, 0, mc.thePlayer);
			setLast = getSysTime();
		} else
			handleSoupSetting();
	}
	
	private Integer getFirstBowl() {
		Minecraft mc = Minecraft.getMinecraft();
		for(int slot = 38; slot <= 44; slot++) {
			Slot itemSlot = mc.thePlayer.inventoryContainer.getSlot(slot);
			if(itemSlot != null) {
				ItemStack stack = itemSlot.getStack();
				if(stack != null && stack.getItem() == Item.getItemById(BOWL_EMPTY))
					return slot;
			}
		}
		return null;
	}
	
	private void handleSoupSetting() {
		Minecraft mc = Minecraft.getMinecraft();
		Integer firstSoup = getFirstSoupFromInventory();
		Integer firstOpenSlot = getFirstOpenSlotFromInventory();
		if(firstSoup != null && firstOpenSlot != null && firstSoup != firstOpenSlot) {
			mc.playerController.windowClick(0, firstSoup, 0, 0, mc.thePlayer);
			mc.playerController.windowClick(0, firstOpenSlot, 0, 0, mc.thePlayer);
			setLast = getSysTime();
		}
	}
	
	private Integer getFirstSoupFromInventory() {
		Minecraft mc = Minecraft.getMinecraft();
		for(int slot = 9; slot <= 35; slot++) {
			Slot itemSlot = mc.thePlayer.inventoryContainer.getSlot(slot);
			if(itemSlot != null) {
				ItemStack stack = itemSlot.getStack();
				if(stack != null && stack.getItem() == Item.getItemById(BOWL_SOUP))
					return slot;
			}
		}
		return null;
	}
	
	private Integer getFirstOpenSlotFromInventory() {
		Minecraft mc = Minecraft.getMinecraft();
		for(int slot = 37; slot <= 44; slot++) {
			Slot itemSlot = mc.thePlayer.inventoryContainer.getSlot(slot);
			if(itemSlot != null) {
				ItemStack stack = itemSlot.getStack();
				if(stack == null)
					return slot;
			}
		}
		return null;
	}
	
	public Integer getSoupCount() {
		Minecraft mc = Minecraft.getMinecraft();
		int count = 0;
		for(int slot = 9; slot <= 44; slot++) {
			Slot itemSlot = mc.thePlayer.inventoryContainer.getSlot(slot);
			if(itemSlot != null) {
				ItemStack stack = itemSlot.getStack();
				if(stack != null && stack.getItem() == Item.getItemById(BOWL_SOUP))
					count++;
			}
		}
		return count;
	}
}
