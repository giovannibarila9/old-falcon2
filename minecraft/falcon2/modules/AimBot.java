package falcon2.modules;

import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.entity.*;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.MathHelper;
import net.minecraft.util.StringUtils;
import falcon2.Falcon2;
import falcon2.modules.events.*;

public class AimBot extends Module {
	private Long changeLast = null, changeDelay = 256L;
	public static Float aimYaw = null, aimPitch = null;

	public AimBot() {
		super("AimBot", null, Category.AURA, false);
	}

	@Override
	public void onEvent(Event event) {
		if (event instanceof EventMotionUpdate) {
			EventMotionUpdate event2 = (EventMotionUpdate) event;
			if (event2.getState() == State.PRE) {
				if (isAuraEnabled()) {
					if (getEnabled() && (!isTargetApproved(Aura.target) || changeLast == null || getSysTime() >= changeDelay + changeLast))
						handleAimbot();
					else if (!getEnabled())
						Aura.target = getBestTarget();
				}
			} else {
				if (isAuraEnabled() && getEnabled()) {
					if (Aura.target != null) {
						faceEntity(Aura.target);
					} else {
						aimPitch = getPlayer().rotationPitch;
						aimYaw = getPlayer().rotationYaw;
					}
				} else {
					aimPitch = getPlayer().rotationPitch;
					aimYaw = getPlayer().rotationYaw;
				}
			}
		} else if (event instanceof EventRotation) {
			EventRotation event2 = (EventRotation) event;
			if (isAuraEnabled() && getEnabled() && aimYaw != null && aimPitch != null)
				event2.setRotation(aimYaw, aimPitch);
		}
	}
	
	private boolean isAuraEnabled() {
		return getModuleByName("Aura").getEnabled();
	}
	
	private void handleAimbot() {
		Aura.target = getBestTarget();
		changeLast = getSysTime();
	}
	
	private boolean isTargetApproved(Entity target) {
		if (target == null)
			return false;
		boolean isLiving = target instanceof EntityLivingBase;
		boolean isNotMe = target != getPlayer();
		boolean isInVisibility = getPlayer().canEntityBeSeen(target);
		boolean isWithinReach = getPlayer().getDistanceToEntity(target) <= Aura.reach;
		boolean isAlive = !target.isDead;
		return isLiving && isNotMe && isInVisibility && isWithinReach && isAlive;
	}

	private void faceEntity(Entity par1Entity) {
		if (aimPitch == null)
			aimPitch = getPlayer().rotationPitch;
		if (aimYaw == null)
			aimYaw = getPlayer().rotationYaw;
		double var4 = par1Entity.posX - getPlayer().posX;
		double var6 = par1Entity.posZ - getPlayer().posZ;
		double var8;
		if (par1Entity instanceof EntityLivingBase) {
			EntityLivingBase var10 = (EntityLivingBase)par1Entity;
			var8 = var10.posY + (double)var10.getEyeHeight() - (getPlayer().posY + (double)getPlayer().getEyeHeight());
		} else {
			var8 = (par1Entity.boundingBox.minY + par1Entity.boundingBox.maxY) / 2.0D - (getPlayer().posY + (double)getPlayer().getEyeHeight());
		}
		double var14 = (double)MathHelper.sqrt_double(var4 * var4 + var6 * var6);
		float var12 = (float)(Math.atan2(var6, var4) * 180.0D / Math.PI) - 90.0F;
		float var13 = (float)(-(Math.atan2(var8, var14) * 180.0D / Math.PI));
		float pitch = updateRotation(aimPitch, var13, 180F);
		float yaw = updateRotation(aimYaw, var12, 180F);
		int speed = 160;
		if (aimPitch < pitch - 5) {
			aimPitch += speed;
			if (aimPitch > pitch + 5)
				aimPitch = pitch;
		}
		if (aimPitch > pitch + 5) {
			aimPitch -= speed;
			if (aimPitch < pitch - 5)
				aimPitch = pitch;
		}
		if (aimYaw < yaw - 5) {
			aimYaw += speed;
			if (aimYaw > yaw + 5)
				aimYaw = yaw;
		}
		if (aimYaw > yaw + 5) {
			aimYaw -= speed;
			if (aimYaw < yaw - 5)
				aimYaw = yaw;
		}
	}

	public static boolean isFacingEntity(Entity par1Entity) {
		double var4 = par1Entity.posX - getPlayer().posX;
		double var6 = par1Entity.posZ - getPlayer().posZ;
		double var8;
		if (par1Entity instanceof EntityLivingBase) {
			EntityLivingBase var10 = (EntityLivingBase)par1Entity;
			var8 = var10.posY + (double)var10.getEyeHeight() - (getPlayer().posY + (double)getPlayer().getEyeHeight());
		} else {
			var8 = (par1Entity.boundingBox.minY + par1Entity.boundingBox.maxY) / 2.0D - (getPlayer().posY + (double)getPlayer().getEyeHeight());
		}
		double var14 = (double)MathHelper.sqrt_double(var4 * var4 + var6 * var6);
		float var12 = (float)(Math.atan2(var6, var4) * 180.0D / Math.PI) - 90.0F;
		float var13 = (float)(-(Math.atan2(var8, var14) * 180.0D / Math.PI));
		float pitch = updateRotation(aimPitch, var13, 180F);
		float yaw = updateRotation(aimYaw, var12, 180F);
		boolean check = false;
		check = aimPitch >= pitch - 10;
		check = aimPitch <= pitch + 10;
		check = aimYaw >= yaw - 30;
		check = aimYaw <= yaw + 30;
		return check;
	}

	private void lookBack() {
		if (aimPitch == null)
			aimPitch = getPlayer().rotationPitch;
		if (aimYaw == null)
			aimYaw = getPlayer().rotationYaw;
		float pitch = getPlayer().rotationPitch;
		float yaw = getPlayer().rotationYaw;
		int speed = 35;
		if (aimPitch < pitch - 10) {
			aimPitch += speed;
			if (aimPitch > pitch + 10)
				aimPitch = pitch;
		}
		if (aimPitch > pitch + 10) {
			aimPitch -= speed;
			if (aimPitch < pitch - 10)
				aimPitch = pitch;
		}
		if (aimYaw < yaw - 10) {
			aimYaw += speed;
			if (aimYaw > yaw + 10)
				aimYaw = yaw;
		}
		if (aimYaw > yaw + 10) {
			aimYaw -= speed;
			if (aimYaw < yaw - 10)
				aimYaw = yaw;
		}
	}

	private static float updateRotation(float par1, float par2, float par3) {
		float var4 = MathHelper.wrapAngleTo180_float(par2 - par1);
		if (var4 > par3)
			var4 = par3;
		if (var4 < -par3)
			var4 = -par3;
		return par1 + var4;
	}
	
	private Entity getBestTarget() {
		Entity best = null;
		for (Entity current : getWorld().loadedEntityList) {
			if (current != null) {
				if (current instanceof EntityLivingBase && !(current instanceof EntityClientPlayerMP)) {
					boolean reachable = getPlayer().getDistanceToEntity(current) <= Aura.reach;
					boolean visible = getPlayer().canEntityBeSeen(current);
					boolean alive = !current.isDead;
					boolean notPlayer = current != getPlayer();
					if (reachable && visible && alive && notPlayer) {
						boolean friendCheck = true;
						if (current instanceof EntityPlayer) {
							String username = StringUtils.stripControlCodes(((EntityPlayer) current).getCommandSenderName());
							friendCheck = !Friends.checkFriend(username);
						}
						if (friendCheck) {
							if (best != null) {
								float currentHealth = ((EntityLivingBase) current).getHealth();
								float bestHealth = ((EntityLivingBase) best).getHealth();
								if (currentHealth < bestHealth)
									best = current;
							} else {
								best = current;
							}
						}
					}
				}
			}
		}
		return best;
	}
}
