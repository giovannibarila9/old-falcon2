package falcon2.modules.events;

public class EventChatRender extends Event {
	private Object[] objects;
	private boolean canceled;
	
	public EventChatRender(Object[] objects) {
		this.objects = objects;
	}
	
	public Object[] getObjects() {
		return objects;
	}
	
	public boolean isCanceled() {
		return canceled;
	}
	
	public void setCanceled(boolean canceled) {
		this.canceled = canceled;
	}
}
