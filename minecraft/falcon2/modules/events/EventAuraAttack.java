package falcon2.modules.events;

public class EventAuraAttack extends Event {
	private State state;
	
	public EventAuraAttack(State state) {
		this.state = state;
	}
	
	public State getState() {
		return state;
	}
}