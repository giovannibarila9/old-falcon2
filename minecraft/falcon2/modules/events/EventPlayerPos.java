package falcon2.modules.events;

public class EventPlayerPos extends Event {
	private double x;
	private double y;
	private double z;
	private double minY;
	
	public EventPlayerPos(double x, double y, double z, double minY) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.minY = minY;
	}
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public double getZ() {
		return z;
	}
	
	public double getMinY() {
		return minY;
	}
	
	public void setX(double x) {
		this.x = x;
	}
	
	public void setY(double y) {
		this.y = y;
	}
	
	public void setZ(double z) {
		this.z = z;
	}
	
	public void setMinY(double minY) {
		this.minY = minY;
	}
}
