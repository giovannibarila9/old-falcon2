package falcon2.modules.events;

public class EventSafeWalk extends Event {
	private Object[] objects;
	private Object[] objectsEdit;
	
	public EventSafeWalk(Object[] objects) {
		this.objects = objects;
	}
	
	public Object[] getObjects() {
		return objects;
	}
	
	public void setObjectsEdit(Object[] objectsEdit) {
		this.objectsEdit = objectsEdit;
	}
	
	public Object[] getObjectsEdit() {
		return objectsEdit;
	}
}
