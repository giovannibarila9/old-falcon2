package falcon2.modules;

import org.lwjgl.input.Keyboard;
import falcon2.Falcon2;
import falcon2.modules.events.*;

public class AutoRespawn extends Module {
	public AutoRespawn() {
		super("AutoRespawn", null, Category.OTHER, false);
	}
	
	@Override
	public void onEvent(Event event) {
		if (!getEnabled())
			return;
		if (event instanceof EventDied) {
			getPlayer().respawnPlayer();
		}
	}
}
