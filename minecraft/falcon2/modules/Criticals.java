package falcon2.modules;

import net.minecraft.network.play.client.C03PacketPlayer;
import falcon2.Falcon2;
import falcon2.modules.events.*;

public class Criticals extends Module {
	private boolean jumped = false;
	private float fallDist = 0.0F;
	
	public Criticals() {
		super("Criticals", null, Category.AURA, false);
	}
	
	@Override
	public void onEvent(Event event) {
		if (!getEnabled())
			return;
		if (getPlayer().capabilities.isFlying) {
			fallDist = 0;
			return;
		}
		if (getGameSettings().keyBindJump.pressed) {
			return;
		}
		if (getPlayer().isCollidedHorizontally)
			return;
		if (event instanceof EventMotionUpdate) {
			if (getModuleByName("Aura").getEnabled() && Aura.target != null) {
				EventMotionUpdate event2 = (EventMotionUpdate) event;
				if (jumped) {
					if (getPlayer().onGround) {
						jumped = false;
					} else {
						return;
					}
				}
				if (event2.getState() == State.PRE) { 
					if (getPlayer().onGround) {
						getPlayer().onGround = false;
						getPlayer().motionY = 0.125D;
						fallDist += 0.125F;
					}
				} else {
					getPlayer().onGround = true;
					getPlayer().motionY = -0.3D;
					if (fallDist >= 3.0F) {
						fallDist = 0.0F;
						getSendQueue().addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(getPlayer().posX, getPlayer().boundingBox.minY + 1D, getPlayer().posY + 1D, getPlayer().posZ, getPlayer().onGround));
						jumped = true;
					}
				}
			}
		}
	}
}
