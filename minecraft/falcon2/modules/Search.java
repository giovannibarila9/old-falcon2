package falcon2.modules;

import java.awt.Color;
import java.util.ArrayList;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.StringUtils;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

import falcon2.Falcon2;
import falcon2.modules.commands.*;
import falcon2.modules.events.*;

public class Search extends Module {
	public static ArrayList<SearchBlock> blocks = new ArrayList<SearchBlock>();
	public static int distance = 30;
	
	public Search() {
		super("Search", Keyboard.KEY_Y, Category.RENDER, false);
	}
	
	@Override
	public void onEvent(Event event) {
		if (!getEnabled())
			return;
		if (event instanceof EventRender) {
			renderSearch();
		}
	}
	
	@Override
	public void loadCommands(CommandManager commandManager) {
		commandManager.getCommands().add(new Command("sh") {
			@Override
			public boolean runCommand(String arguments) {
				if (arguments.contains(" ")) {
					String args[] = arguments.split(" ");
					if (args[0].equalsIgnoreCase("add")) {
						int blockID = Integer.parseInt(args[1]);
						if (!listContainsID(blockID)) {
							String colors[] = args[2].split(",");
							float red = (float) Integer.parseInt(colors[0]);
							float green = (float) Integer.parseInt(colors[1]);
							float blue = (float) Integer.parseInt(colors[2]);
							if (red < 0F)
								red = 0F;
							if (green < 0F)
								green = 0F;
							if (blue < 0F)
								blue = 0F;
							if (red > 255F)
								red = 255F;
							if (green > 255F)
								green = 255F;
							if (blue > 255F)
								blue = 255F;
							Color color = new Color(red / 255F, green / 255F, blue / 255F);
							blocks.add(new SearchBlock(blockID, color));
							String split = "\247f, \247e";
							String colorString = ((int) red) + split + ((int) green) + split + ((int) blue);
							addChatMessage("Added block '\247e" + blockID + "\247f' with color '\247e" + colorString + "\247f' to Search.");
							Falcon2.getFileManager().saveFile(Falcon2.getFileManager().getFileInstances().fileHandlerSearch);
						} else {
							addChatMessageError("Block '\247e" + blockID + "\247f' is already on Search.");
						}
						return true;
					} else if (args[0].equalsIgnoreCase("distance")) {
						distance = Integer.parseInt(args[1]);
						addChatMessage("Search distance set to '\247e" + distance + "\247f'.");
						Falcon2.getFileManager().saveFile(Falcon2.getFileManager().getFileInstances().fileHandlerSettings);
						return true;
					} else if (args[0].equalsIgnoreCase("rem")) {
						int blockID = Integer.parseInt(args[1]);
						if (listContainsID(blockID)) {
							blocks.remove(getBlockByID(blockID));
							addChatMessage("Removed block '\247e" + blockID + "\247f' from Search.");
							Falcon2.getFileManager().saveFile(Falcon2.getFileManager().getFileInstances().fileHandlerSearch);
						} else {
							addChatMessageError("Block '\247e" + blockID + "\247f' isn't on Search.");
						}
						return true;
					}
				}
				return false;
			}
		});
	}
	
	private void renderSearch() {
		if (blocks.isEmpty())
			return;
		int posX = (int) getPlayer().posX;
		int posY = (int) getPlayer().posY;
		int posZ = (int) getPlayer().posZ;
		int distance = this.distance / 2;
		for (int x = (posX - distance); x < (posX + distance); x++) {
			for (int y = (posY - distance); y < (posY + distance); y++) {
				for (int z = (posZ - distance); z < (posZ + distance); z++) {
					int blockID = Block.getIdFromBlock(getWorld().getBlock(x, y, z));
					if (listContainsID(blockID)) {
						SearchBlock block = getBlockByID(blockID);
						drawBox(x, y, z, block.getColor());
					}
				}
			}
		}
	}
	
	private boolean listContainsID(int id) {
		for (SearchBlock sBlock : blocks) {
			if (sBlock.getID() == id)
				return true;
		}
		return false;
	}
	
	private SearchBlock getBlockByID(int id) {
		for (SearchBlock sBlock : blocks) {
			if (sBlock.getID() == id)
				return sBlock;
		}
		return null;
	}

	private void drawBox(int x, int y, int z, Color color) {
		float xDiff = (float) (x - RenderManager.renderPosX);
		float yDiff = (float) (y - RenderManager.renderPosY);
		float zDiff = (float) (z - RenderManager.renderPosZ);
		renderUtils.start3DGLConstants();
		GL11.glEnable(GL11.GL_LINE_SMOOTH);
		GL11.glHint(GL11.GL_LINE_SMOOTH_HINT, GL11.GL_FASTEST);
		float red = color.getRed();
		float green = color.getGreen();
		float blue = color.getBlue();
		float alpha = 0.4F;
		GL11.glColor4f(red, green, blue, alpha);
		GL11.glLineWidth(1.5F);
		float width = 1F;
		float height = 1F;
		AxisAlignedBB axisAlignedBB = AxisAlignedBB.getBoundingBox(xDiff, yDiff, zDiff, xDiff + width, yDiff + height, zDiff + width);
		GL11.glPushMatrix();
		renderUtils.drawOutlinedBoundingBox(axisAlignedBB);
		alpha -= 0.2F;
		GL11.glColor4f(red, green, blue, alpha);
		renderUtils.drawBoundingBox(axisAlignedBB);
		GL11.glPopMatrix();
		GL11.glDisable(GL11.GL_LINE_SMOOTH);
		renderUtils.finish3DGLConstants();
	}
	
	public static class SearchBlock {
		private int id;
		private Color color;
		
		public SearchBlock(int id, Color color) {
			this.id = id;
			this.color = color;
		}
		
		public int getID() {
			return id;
		}
		
		public Color getColor() {
			return color;
		}
	}
}
