package net.minecraft.enchantment;

public class EnchantmentArrowFire extends Enchantment {
   private static final String __OBFID = "CL_00000099";

   public EnchantmentArrowFire(int par1, int par2) {
      super(par1, par2, EnumEnchantmentType.bow);
      this.setName("arrowFire");
   }

   public int getMinEnchantability(int par1) {
      return 20;
   }

   public int getMaxEnchantability(int par1) {
      return 50;
   }

   public int getMaxLevel() {
      return 1;
   }
}
