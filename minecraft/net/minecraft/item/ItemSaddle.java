package net.minecraft.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.passive.EntityPig;
import net.minecraft.entity.player.EntityPlayer;

public class ItemSaddle extends Item {
   private static final String __OBFID = "CL_00000059";

   public ItemSaddle() {
      this.maxStackSize = 1;
      this.setCreativeTab(CreativeTabs.tabTransport);
   }

   public boolean itemInteractionForEntity(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, EntityLivingBase par3EntityLivingBase) {
      if(par3EntityLivingBase instanceof EntityPig) {
         EntityPig var4 = (EntityPig)par3EntityLivingBase;
         if(!var4.getSaddled() && !var4.isChild()) {
            var4.setSaddled(true);
            var4.worldObj.playSoundAtEntity(var4, "mob.horse.leather", 0.5F, 1.0F);
            --par1ItemStack.stackSize;
         }

         return true;
      } else {
         return false;
      }
   }

   public boolean hitEntity(ItemStack par1ItemStack, EntityLivingBase par2EntityLivingBase, EntityLivingBase par3EntityLivingBase) {
      this.itemInteractionForEntity(par1ItemStack, (EntityPlayer)null, par2EntityLivingBase);
      return true;
   }
}
